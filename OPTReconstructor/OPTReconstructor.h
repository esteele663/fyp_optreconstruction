#ifndef OPT_RECONSTRUCTOR_H
#define OPT_RECONSTRUCTOR_H

#include <QtWidgets/QMainWindow>
#include <QRunnable.h>
#include "ui_OPTReconstructor.h"

#include <opencv2\opencv.hpp>

#include <vector>

//#define OPT_SINGLE_THREAD
//#define OPT_OPEN_MP
#define OPT_CUDA

class OPTReconstructor : public QMainWindow
{
	Q_OBJECT

public:
	OPTReconstructor(QWidget *parent = Q_NULLPTR);

private:
	Ui::OPTReconstructorClass ui;
	std::vector<cv::Mat> m_imageData;
	bool m_loaded;
	bool m_original16Bit;

	void Reconstruct();

#ifdef OPT_CUDA
	bool handleCudaError(bool isSuccess);
#endif //OPT_CUDA

signals:
	void progressUpdated(int value);
	void reconstructionCompleted();
	void sliceCompleted();
	void outputMessage(QString message);

private slots:
	void OnBrowseButtonClicked(QLineEdit* out, bool directory);
	void OnLoadAcquisition();
	void OnChangeImage(int n);
	void OnUpdateSteps();
	void OnReconstruct();
	void OnReonstructionCompleted();
	void OnSliceCompleted();
	void OnOutputMessage(QString message);
};

#endif //!OPT_RECONSTRUCTOR_H
