#define _USE_MATH_DEFINES
#include "OPTReconstructor.h"
#include <filesystem>
#include <algorithm>
#include <cmath>
#include <sstream>

#include <QFileDialog.h>
#include <QMessageBox.h>
#include <QImageWriter.h>
#include <QtConcurrent\QtConcurrentRun.h>


#ifdef OPT_OPEN_MP
#include <omp.h>
#endif //OPT_OPEN_MP

#ifdef OPT_CUDA
#include <OPTReconstruct_CUDA.h>
#endif OPT_CUDA

#ifndef OPT_CUDA
#define TO_RAD(x) ((x) * M_PI / 180)
#define XY(x, y, width) ((y) * width + (x))
#define XYZ(x, y, z, width, height) ((z) * (width) * (height) + (y) * (width) + (x))
#endif

#ifdef _WIN32
#define fs std::experimental::filesystem
#else
#define fs std::filesystem
#endif

OPTReconstructor::OPTReconstructor(QWidget *parent)
	: QMainWindow(parent),
	m_loaded(false)
{
	ui.setupUi(this);
	
	ui.reconstructionProgress->setValue(0);

	connect(ui.btnBrowseAcquisition, &QPushButton::clicked, this, [this] { OnBrowseButtonClicked(ui.txtAcquisitionFileLocation, true); });
	connect(ui.btnBrowsePositionFile, &QPushButton::clicked, this, [this] { OnBrowseButtonClicked(ui.txtPositionFileLocation, false); });
	connect(ui.btnLoad, SIGNAL(clicked()), this, SLOT(OnLoadAcquisition()));
	connect(ui.imageNumberSlider, SIGNAL(valueChanged(int)), this, SLOT(OnChangeImage(int)));
	connect(ui.spnNumSpaces, SIGNAL(valueChanged(int)), this, SLOT(OnUpdateSteps()));
	connect(ui.btnReconstruct, SIGNAL(clicked()), this, SLOT(OnReconstruct()));
	connect(this, SIGNAL(progressUpdated(int)), ui.reconstructionProgress, SLOT(setValue(int)));
	connect(this, SIGNAL(reconstructionCompleted()), this, SLOT(OnReonstructionCompleted()));
	connect(this, SIGNAL(sliceCompleted()), this, SLOT(OnSliceCompleted()));
	connect(this, SIGNAL(outputMessage(QString)), this, SLOT(OnOutputMessage(QString)));
}

void OPTReconstructor::OnBrowseButtonClicked(QLineEdit* out, bool directory)
{
	QString filename;
	if (directory)
		filename = QFileDialog::getExistingDirectory(this, tr("Select Input Directory"));
	else
		filename = QFileDialog::getOpenFileName(this, tr("Select Positions CSV"), QString(), tr("CSV Files (*.csv)"));
	out->setText(filename);
}

void OPTReconstructor::OnLoadAcquisition()
{
	QString dirName = ui.txtAcquisitionFileLocation->text();
	if (dirName == "")
		return;

	std::vector<QString> files;
	for (auto &p : fs::directory_iterator(dirName.toStdString()))
	{
		files.push_back(QString::fromStdString(p.path().string()));
	}

	auto it = std::remove_if(files.begin(), files.end(), [](QString name) { return !name.endsWith(".tif"); });
	files.erase(it, files.end());
	std::sort(files.begin(), files.end(), std::less<QString>());

	m_original16Bit = false;
	for (auto &name : files)
	{
		cv::Mat imIn;
		cv::Mat image;

		imIn = cv::imread(name.toStdString(), cv::IMREAD_GRAYSCALE | cv::IMREAD_ANYDEPTH);

		if (imIn.type() == CV_16UC1)
			m_original16Bit = true;

		imIn.convertTo(image, CV_32FC1, m_original16Bit ? 1/65535.0 : 1.0);


		m_imageData.push_back(image);
	}

	cv::Mat showIm;
	if (m_original16Bit)
		m_imageData[0].convertTo(showIm, CV_8UC1, 255.0);
	else
		m_imageData[0].convertTo(showIm, CV_8UC1);

	ui.lblImages->setPixmap(QPixmap::fromImage(QImage(showIm.data, showIm.cols, showIm.rows, QImage::Format_Grayscale8)));
	ui.imageNumberSlider->setMaximum(m_imageData.size() - 1);
	ui.spnNumSpaces->setValue(m_imageData.size());

	ui.reconstructionProgress->setMaximum(m_imageData[0].rows);

	m_loaded = true;
}

void OPTReconstructor::OnChangeImage(int n)
{
	cv::Mat showIm;

	if (m_original16Bit)
		m_imageData[n].convertTo(showIm, CV_8UC1, 255.0);
	else
		m_imageData[n].convertTo(showIm, CV_8UC1);
	ui.lblImages->setPixmap(QPixmap::fromImage(QImage(showIm.data, showIm.cols, showIm.rows, QImage::Format_Grayscale8)));
}

void OPTReconstructor::OnUpdateSteps()
{
	ui.positionsList->clear();
	float dTheta = 360.0 / ui.spnNumSpaces->value();
	for (int i = 0; i < ui.spnNumSpaces->value(); i++)
		ui.positionsList->addItem(QString::number(i*dTheta));
}

void OPTReconstructor::OnReconstruct()
{
	QtConcurrent::run([this]() { Reconstruct(); });
}

void OPTReconstructor::OnReonstructionCompleted()
{
	QMessageBox qmb;
	qmb.setText("Reconstruction Completed");
	ui.reconstructionProgress->setValue(m_imageData[0].rows);
	qmb.exec();
}

#if defined(OPT_OPEN_MP) || defined(OPT_SINGLE_THREAD)
void OPTReconstructor::Reconstruct()
{
	if (!m_loaded)
		return;

	int sz = m_imageData[0].cols;
	const double dTheta = 360.0 / m_imageData.size();

	float *out = new float[sz*sz*m_imageData[0].rows]();
	if (out == nullptr) return;
	
	float min =   std::numeric_limits<double>::infinity();
	float max = - std::numeric_limits<double>::infinity();

#ifdef OPT_OPEN_MP
	#pragma omp parallel for
#endif // OPT_OPEN_MP
	for (int z = 0; z < m_imageData[0].rows; z++)
	{
		for (int t = 0; t < m_imageData.size(); t++)
		{
			for (int x = -sz / 2; x < sz / 2; x++)
			{
				for (int y = -sz / 2; y < sz / 2; y++)
				{
					double r = x * std::sin(TO_RAD(t*dTheta)) - y * std::cos(TO_RAD(t*dTheta));
					int ri = (int)std::round((sz - 1)*(r / std::sqrt(2) + sz / 2) / sz);

					uint8_t *image = (uint8_t*)m_imageData[t].data;
					out[XYZ(x + sz / 2, y + sz / 2, z, sz, sz)] += image[XY(ri, z, sz)];
				}
			}
		}

		double progress = (100.0*z) / (double)m_imageData[0].rows;
		emit sliceCompleted();
	}

	for (int i = 0; i < sz*sz*m_imageData[0].rows; i++)
	{
		max = std::max(max, out[i]);
		min = std::min(min, out[i]);
	}

	fs::path dirPath = fs::path((ui.txtAcquisitionFileLocation->text() + "/recon").toStdString());
	if (!fs::exists(dirPath))
		fs::create_directory(dirPath);

	for (int z = 0; z < m_imageData[0].rows; z++)
	{
		cv::Mat tmp(sz, sz, CV_32FC1, out + z*sz*sz);
		cv::Mat outImage;

		tmp.convertTo(outImage, CV_16UC1, 65535.0 / (max - min), -min);

		std::stringstream ss;
		ss << dirPath.string() << "/" << "out" << std::setfill('0') << std::setw(3) << z << ".tif";
		cv::imwrite(ss.str(), outImage);
	}

	emit reconstructionCompleted();

	delete[] out;
}
#endif //OPT_SINGLE_THREAD || OPT_OPEN_MP


#ifdef OPT_CUDA
void OPTReconstructor::Reconstruct()
{
	bool failed = false;

	int numSlices = std::min(OPTCUDA_GetMaxSlices(m_imageData[0].cols, m_imageData.size()), (int)m_imageData[0].rows);

	OPTCUDA_InitData initData;
	initData.imWidth = m_imageData[0].cols;
	initData.imHeight = numSlices;//m_imageData[0].rows;
	initData.nTheta = m_imageData.size();
	initData.progressCallback = [](void * self, int progress) { emit((OPTReconstructor*)self)->progressUpdated(progress); };
	initData.callbackInfo = (void *)this;
	initData.outWidth = initData.imWidth;
	initData.outHeight = initData.imWidth;

	float * reconstruction = new float[initData.outWidth*initData.outHeight*m_imageData[0].rows];
	initData.output_buffer = reconstruction;

	if (!handleCudaError(OPTCUDA_Initialize(&initData)))
	{
		failed = true;
		goto OPTCUDA_DEINIT;
	}

	for (int offset = 0; offset < m_imageData[0].rows; offset += numSlices)
	{
		int nSlices = std::min((int)(m_imageData[0].rows - offset), numSlices);
		for (int i = 0; i < m_imageData.size(); i++)
		{
			if (!handleCudaError(OPTCUDA_AddImage((float*)m_imageData[i].data, offset, nSlices)))
			{
				failed = true;
				goto OPTCUDA_DEINIT;
			}
		}

		if (!handleCudaError(OPTCUDA_Reconstruct(nSlices)))
		{
			failed = true;
			goto OPTCUDA_DEINIT;
		}
	}

OPTCUDA_DEINIT:
	if (!OPTCUDA_Deinitialize())
	{
		failed = true;
		emit outputMessage("Could not shutdown CUDA!");
	}

	if (!failed)
	{

		float min = std::numeric_limits<double>::infinity();
		float max = -std::numeric_limits<double>::infinity();
		for (int slice = 0; slice < m_imageData[0].rows; slice++)
		{
			float *data = OPTCUDA_GetOutputSlice(slice);
			for (int i = 0; i < initData.outWidth*initData.outHeight; i++)
			{
				max = std::max(max, data[i]);
				min = std::min(min, data[i]);
			}
		}

		fs::path dirPath = fs::path((ui.txtAcquisitionFileLocation->text() + "/recon").toStdString());
		if (!fs::exists(dirPath))
			fs::create_directory(dirPath);


		for (int slice = 0; slice < m_imageData[0].rows; slice++)
		{
			cv::Mat floatIm(initData.outHeight, initData.outWidth, CV_32FC1, OPTCUDA_GetOutputSlice(slice));
			cv::Mat outImage;

			floatIm.convertTo(outImage, CV_16UC1, 65535.0 / (max - min), -min);

			std::stringstream ss;
			ss << dirPath.string() << "/" << "out" << std::setfill('0') << std::setw(3) << slice << ".tif";
			cv::imwrite(ss.str(), outImage);
		}
		emit reconstructionCompleted();
	}

	delete[] reconstruction;
}

bool OPTReconstructor::handleCudaError(bool isSuccess)
{
	if (isSuccess)
		return true;

	char errorString[1024];
	OPTCUDA_GetLastError(errorString, 1024);

	emit outputMessage(QString(errorString));

	return false;
}
#endif //OPT_CUDA

void OPTReconstructor::OnSliceCompleted()
{
	static int slicesCompleted = 0;
	slicesCompleted++;
	emit progressUpdated(slicesCompleted);
}

void OPTReconstructor::OnOutputMessage(QString message)
{
	QMessageBox qmb;
	qmb.setText(message);
	qmb.exec();
}
