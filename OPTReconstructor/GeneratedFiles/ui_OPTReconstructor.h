/********************************************************************************
** Form generated from reading UI file 'OPTReconstructor.ui'
**
** Created by: Qt User Interface Compiler version 5.10.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPTRECONSTRUCTOR_H
#define UI_OPTRECONSTRUCTOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OPTReconstructorClass
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLayout_3;
    QWidget *widget;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *btnReconstruct;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QFrame *frame;
    QGridLayout *gridLayout;
    QLabel *label;
    QLineEdit *txtAcquisitionFileLocation;
    QPushButton *btnBrowseAcquisition;
    QPushButton *btnLoad;
    QFrame *line;
    QLabel *lblImages;
    QSlider *imageNumberSlider;
    QSpacerItem *verticalSpacer;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_2;
    QFrame *frame_2;
    QFormLayout *formLayout;
    QRadioButton *rbUniformSpacing;
    QRadioButton *rbFromFile;
    QSpinBox *spnNumSpaces;
    QFrame *frame_3;
    QGridLayout *gridLayout_2;
    QLabel *lblFileLocation;
    QLineEdit *txtPositionFileLocation;
    QPushButton *btnBrowsePositionFile;
    QListWidget *positionsList;
    QProgressBar *reconstructionProgress;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *OPTReconstructorClass)
    {
        if (OPTReconstructorClass->objectName().isEmpty())
            OPTReconstructorClass->setObjectName(QStringLiteral("OPTReconstructorClass"));
        OPTReconstructorClass->resize(801, 456);
        centralWidget = new QWidget(OPTReconstructorClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLayout_3 = new QGridLayout(centralWidget);
        gridLayout_3->setSpacing(6);
        gridLayout_3->setContentsMargins(11, 11, 11, 11);
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        widget = new QWidget(centralWidget);
        widget->setObjectName(QStringLiteral("widget"));
        horizontalLayout = new QHBoxLayout(widget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        btnReconstruct = new QPushButton(widget);
        btnReconstruct->setObjectName(QStringLiteral("btnReconstruct"));

        horizontalLayout->addWidget(btnReconstruct);


        gridLayout_3->addWidget(widget, 2, 1, 1, 1);

        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        frame = new QFrame(groupBox);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        gridLayout = new QGridLayout(frame);
        gridLayout->setSpacing(6);
        gridLayout->setContentsMargins(11, 11, 11, 11);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        label = new QLabel(frame);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 0, 0, 1, 1);

        txtAcquisitionFileLocation = new QLineEdit(frame);
        txtAcquisitionFileLocation->setObjectName(QStringLiteral("txtAcquisitionFileLocation"));

        gridLayout->addWidget(txtAcquisitionFileLocation, 0, 1, 1, 1);

        btnBrowseAcquisition = new QPushButton(frame);
        btnBrowseAcquisition->setObjectName(QStringLiteral("btnBrowseAcquisition"));

        gridLayout->addWidget(btnBrowseAcquisition, 0, 2, 1, 1);

        btnLoad = new QPushButton(frame);
        btnLoad->setObjectName(QStringLiteral("btnLoad"));

        gridLayout->addWidget(btnLoad, 1, 2, 1, 1);


        verticalLayout->addWidget(frame);

        line = new QFrame(groupBox);
        line->setObjectName(QStringLiteral("line"));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(line);

        lblImages = new QLabel(groupBox);
        lblImages->setObjectName(QStringLiteral("lblImages"));

        verticalLayout->addWidget(lblImages);

        imageNumberSlider = new QSlider(groupBox);
        imageNumberSlider->setObjectName(QStringLiteral("imageNumberSlider"));
        imageNumberSlider->setOrientation(Qt::Horizontal);
        imageNumberSlider->setTickPosition(QSlider::NoTicks);

        verticalLayout->addWidget(imageNumberSlider);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        gridLayout_3->addWidget(groupBox, 0, 0, 1, 1);

        groupBox_2 = new QGroupBox(centralWidget);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        verticalLayout_2 = new QVBoxLayout(groupBox_2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        frame_2 = new QFrame(groupBox_2);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(frame_2->sizePolicy().hasHeightForWidth());
        frame_2->setSizePolicy(sizePolicy);
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        formLayout = new QFormLayout(frame_2);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setSizeConstraint(QLayout::SetMinimumSize);
        formLayout->setContentsMargins(-1, -1, -1, 0);
        rbUniformSpacing = new QRadioButton(frame_2);
        rbUniformSpacing->setObjectName(QStringLiteral("rbUniformSpacing"));
        rbUniformSpacing->setChecked(true);

        formLayout->setWidget(0, QFormLayout::LabelRole, rbUniformSpacing);

        rbFromFile = new QRadioButton(frame_2);
        rbFromFile->setObjectName(QStringLiteral("rbFromFile"));

        formLayout->setWidget(1, QFormLayout::LabelRole, rbFromFile);

        spnNumSpaces = new QSpinBox(frame_2);
        spnNumSpaces->setObjectName(QStringLiteral("spnNumSpaces"));
        spnNumSpaces->setMinimum(1);
        spnNumSpaces->setMaximum(1600);
        spnNumSpaces->setSingleStep(100);
        spnNumSpaces->setValue(100);

        formLayout->setWidget(0, QFormLayout::FieldRole, spnNumSpaces);


        verticalLayout_2->addWidget(frame_2);

        frame_3 = new QFrame(groupBox_2);
        frame_3->setObjectName(QStringLiteral("frame_3"));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        gridLayout_2 = new QGridLayout(frame_3);
        gridLayout_2->setSpacing(6);
        gridLayout_2->setContentsMargins(11, 11, 11, 11);
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setContentsMargins(-1, 0, -1, -1);
        lblFileLocation = new QLabel(frame_3);
        lblFileLocation->setObjectName(QStringLiteral("lblFileLocation"));

        gridLayout_2->addWidget(lblFileLocation, 0, 0, 1, 1);

        txtPositionFileLocation = new QLineEdit(frame_3);
        txtPositionFileLocation->setObjectName(QStringLiteral("txtPositionFileLocation"));

        gridLayout_2->addWidget(txtPositionFileLocation, 0, 1, 1, 1);

        btnBrowsePositionFile = new QPushButton(frame_3);
        btnBrowsePositionFile->setObjectName(QStringLiteral("btnBrowsePositionFile"));

        gridLayout_2->addWidget(btnBrowsePositionFile, 0, 2, 1, 1);


        verticalLayout_2->addWidget(frame_3);

        positionsList = new QListWidget(groupBox_2);
        positionsList->setObjectName(QStringLiteral("positionsList"));

        verticalLayout_2->addWidget(positionsList);


        gridLayout_3->addWidget(groupBox_2, 0, 1, 1, 1);

        reconstructionProgress = new QProgressBar(centralWidget);
        reconstructionProgress->setObjectName(QStringLiteral("reconstructionProgress"));
        reconstructionProgress->setValue(24);

        gridLayout_3->addWidget(reconstructionProgress, 1, 1, 1, 1);

        OPTReconstructorClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(OPTReconstructorClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 801, 26));
        OPTReconstructorClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(OPTReconstructorClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        OPTReconstructorClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(OPTReconstructorClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        OPTReconstructorClass->setStatusBar(statusBar);

        retranslateUi(OPTReconstructorClass);

        QMetaObject::connectSlotsByName(OPTReconstructorClass);
    } // setupUi

    void retranslateUi(QMainWindow *OPTReconstructorClass)
    {
        OPTReconstructorClass->setWindowTitle(QApplication::translate("OPTReconstructorClass", "OPTReconstructor", nullptr));
        btnReconstruct->setText(QApplication::translate("OPTReconstructorClass", "Reconstruct", nullptr));
        groupBox->setTitle(QApplication::translate("OPTReconstructorClass", "Acquisition Data", nullptr));
        label->setText(QApplication::translate("OPTReconstructorClass", "File Location", nullptr));
        btnBrowseAcquisition->setText(QApplication::translate("OPTReconstructorClass", "Browse", nullptr));
        btnLoad->setText(QApplication::translate("OPTReconstructorClass", "Load", nullptr));
        lblImages->setText(QString());
        groupBox_2->setTitle(QApplication::translate("OPTReconstructorClass", "Positions", nullptr));
        rbUniformSpacing->setText(QApplication::translate("OPTReconstructorClass", "Uniform Spacing", nullptr));
        rbFromFile->setText(QApplication::translate("OPTReconstructorClass", "From File", nullptr));
        lblFileLocation->setText(QApplication::translate("OPTReconstructorClass", "File Location", nullptr));
        btnBrowsePositionFile->setText(QApplication::translate("OPTReconstructorClass", "Browse", nullptr));
    } // retranslateUi

};

namespace Ui {
    class OPTReconstructorClass: public Ui_OPTReconstructorClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPTRECONSTRUCTOR_H
