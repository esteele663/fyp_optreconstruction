/****************************************************************************
** Meta object code from reading C++ file 'OPTReconstructor.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.10.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../OPTReconstructor.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'OPTReconstructor.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.10.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_OPTReconstructor_t {
    QByteArrayData data[20];
    char stringdata0[268];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_OPTReconstructor_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_OPTReconstructor_t qt_meta_stringdata_OPTReconstructor = {
    {
QT_MOC_LITERAL(0, 0, 16), // "OPTReconstructor"
QT_MOC_LITERAL(1, 17, 15), // "progressUpdated"
QT_MOC_LITERAL(2, 33, 0), // ""
QT_MOC_LITERAL(3, 34, 5), // "value"
QT_MOC_LITERAL(4, 40, 23), // "reconstructionCompleted"
QT_MOC_LITERAL(5, 64, 14), // "sliceCompleted"
QT_MOC_LITERAL(6, 79, 13), // "outputMessage"
QT_MOC_LITERAL(7, 93, 7), // "message"
QT_MOC_LITERAL(8, 101, 21), // "OnBrowseButtonClicked"
QT_MOC_LITERAL(9, 123, 10), // "QLineEdit*"
QT_MOC_LITERAL(10, 134, 3), // "out"
QT_MOC_LITERAL(11, 138, 9), // "directory"
QT_MOC_LITERAL(12, 148, 17), // "OnLoadAcquisition"
QT_MOC_LITERAL(13, 166, 13), // "OnChangeImage"
QT_MOC_LITERAL(14, 180, 1), // "n"
QT_MOC_LITERAL(15, 182, 13), // "OnUpdateSteps"
QT_MOC_LITERAL(16, 196, 13), // "OnReconstruct"
QT_MOC_LITERAL(17, 210, 24), // "OnReonstructionCompleted"
QT_MOC_LITERAL(18, 235, 16), // "OnSliceCompleted"
QT_MOC_LITERAL(19, 252, 15) // "OnOutputMessage"

    },
    "OPTReconstructor\0progressUpdated\0\0"
    "value\0reconstructionCompleted\0"
    "sliceCompleted\0outputMessage\0message\0"
    "OnBrowseButtonClicked\0QLineEdit*\0out\0"
    "directory\0OnLoadAcquisition\0OnChangeImage\0"
    "n\0OnUpdateSteps\0OnReconstruct\0"
    "OnReonstructionCompleted\0OnSliceCompleted\0"
    "OnOutputMessage"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_OPTReconstructor[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       4,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    1,   74,    2, 0x06 /* Public */,
       4,    0,   77,    2, 0x06 /* Public */,
       5,    0,   78,    2, 0x06 /* Public */,
       6,    1,   79,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       8,    2,   82,    2, 0x08 /* Private */,
      12,    0,   87,    2, 0x08 /* Private */,
      13,    1,   88,    2, 0x08 /* Private */,
      15,    0,   91,    2, 0x08 /* Private */,
      16,    0,   92,    2, 0x08 /* Private */,
      17,    0,   93,    2, 0x08 /* Private */,
      18,    0,   94,    2, 0x08 /* Private */,
      19,    1,   95,    2, 0x08 /* Private */,

 // signals: parameters
    QMetaType::Void, QMetaType::Int,    3,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    7,

 // slots: parameters
    QMetaType::Void, 0x80000000 | 9, QMetaType::Bool,   10,   11,
    QMetaType::Void,
    QMetaType::Void, QMetaType::Int,   14,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    7,

       0        // eod
};

void OPTReconstructor::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        OPTReconstructor *_t = static_cast<OPTReconstructor *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->progressUpdated((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->reconstructionCompleted(); break;
        case 2: _t->sliceCompleted(); break;
        case 3: _t->outputMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 4: _t->OnBrowseButtonClicked((*reinterpret_cast< QLineEdit*(*)>(_a[1])),(*reinterpret_cast< bool(*)>(_a[2]))); break;
        case 5: _t->OnLoadAcquisition(); break;
        case 6: _t->OnChangeImage((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 7: _t->OnUpdateSteps(); break;
        case 8: _t->OnReconstruct(); break;
        case 9: _t->OnReonstructionCompleted(); break;
        case 10: _t->OnSliceCompleted(); break;
        case 11: _t->OnOutputMessage((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 4:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< QLineEdit* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        {
            typedef void (OPTReconstructor::*_t)(int );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&OPTReconstructor::progressUpdated)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (OPTReconstructor::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&OPTReconstructor::reconstructionCompleted)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (OPTReconstructor::*_t)();
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&OPTReconstructor::sliceCompleted)) {
                *result = 2;
                return;
            }
        }
        {
            typedef void (OPTReconstructor::*_t)(QString );
            if (*reinterpret_cast<_t *>(_a[1]) == static_cast<_t>(&OPTReconstructor::outputMessage)) {
                *result = 3;
                return;
            }
        }
    }
}

const QMetaObject OPTReconstructor::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_OPTReconstructor.data,
      qt_meta_data_OPTReconstructor,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *OPTReconstructor::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *OPTReconstructor::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_OPTReconstructor.stringdata0))
        return static_cast<void*>(this);
    return QMainWindow::qt_metacast(_clname);
}

int OPTReconstructor::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    }
    return _id;
}

// SIGNAL 0
void OPTReconstructor::progressUpdated(int _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void OPTReconstructor::reconstructionCompleted()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void OPTReconstructor::sliceCompleted()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}

// SIGNAL 3
void OPTReconstructor::outputMessage(QString _t1)
{
    void *_a[] = { nullptr, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
