#include "uk_co_cairn_CuOPT_CuOPT_JNI.h"
#include <OPTReconstruct_CUDA.h>
#include <string>
#include <sstream>

#include <Windows.h>

bool initialised = false;
std::string errorString = "";


struct JVM_CallbackInfo
{
	JavaVM *g_vm = nullptr;
	jobject g_this = nullptr;
	jmethodID g_methodID = nullptr;
} ci;

void RunCallback(void *data, int i) 
{
	JVM_CallbackInfo *ci = (JVM_CallbackInfo*)data;
	JNIEnv *env;
	int res = ci->g_vm->GetEnv((void **)&env, JNI_VERSION_1_6);
	if (res == JNI_EDETACHED) 
	{
		if (ci->g_vm->AttachCurrentThread((void **)&env, NULL) != 0) {
			env->CallVoidMethod(ci->g_this, ci->g_methodID, i);
			ci->g_vm->DetachCurrentThread();
		}
	}
	else if (res == JNI_OK)
	{
		env->CallVoidMethod(ci->g_this, ci->g_methodID, i);
	}
}

JNIEXPORT jboolean JNICALL Java_uk_co_cairn_CuOPT_CuOPT_1JNI_Initialize(JNIEnv * env, jobject thisObj, jobject initDataObj)
{
	if (initialised)
	{
		errorString = "JNI Error: Already initialised!";
		return JNI_FALSE;
	}

	jclass initDataClass = env->GetObjectClass(initDataObj);

	jfieldID widthID = env->GetFieldID(initDataClass, "width", "I");
	if (widthID == nullptr)
	{
		errorString = "JNI Error: Could not find width ID!";
		return JNI_FALSE;
	}

	jfieldID heightID = env->GetFieldID(initDataClass, "height", "I");
	if (heightID == nullptr)
	{
		errorString = "JNI Error: Could not find height ID!";
		return JNI_FALSE;
	}

	jfieldID nThetaID = env->GetFieldID(initDataClass, "nTheta", "I");
	if (nThetaID == nullptr)
	{
		errorString = "JNI Error: Could not find nTheta ID!";
		return JNI_FALSE;
	}

	jfieldID outWidthID = env->GetFieldID(initDataClass, "outWidth", "I");
	if (outWidthID == nullptr)
	{
		errorString = "JNI Error: Could not find outWidth ID!";
		return JNI_FALSE;
	}

	jfieldID outHeightID = env->GetFieldID(initDataClass, "outHeight", "I");
	if (outHeightID == nullptr)
	{
		errorString = "JNI Error: Could not find outHeight ID!";
		return JNI_FALSE;
	}

	jfieldID outputBufferID = env->GetFieldID(initDataClass, "outBuffer", "Ljava/nio/ByteBuffer;");
	if (outputBufferID == nullptr)
	{
		errorString = "JNI Error: Could not find outputBuffer ID!";
		return JNI_FALSE;
	}

	ci.g_this = env->NewGlobalRef(thisObj);
	jclass g_class = env->GetObjectClass(ci.g_this);
	if (g_class == nullptr)
	{
		errorString = "JNI Error: Could not find java class!";
		return JNI_FALSE;
	}

	ci.g_methodID = env->GetMethodID(g_class, "RunCallback", "(I)V");
	if (ci.g_methodID == nullptr) 
	{
		errorString = "JNI Error: Could not find callback method!";
		return JNI_FALSE;
	}

	env->GetJavaVM(&ci.g_vm);

	OPTCUDA_InitData id = {};
	id.imWidth = env->GetIntField(initDataObj, widthID);
	id.imHeight = env->GetIntField(initDataObj, heightID);
	id.nTheta = env->GetIntField(initDataObj, nThetaID);
	id.outWidth = env->GetIntField(initDataObj, outWidthID);
	id.outHeight = env->GetIntField(initDataObj, outHeightID);
	id.output_buffer = (float*)env->GetDirectBufferAddress(env->GetObjectField(initDataObj, outputBufferID));
	id.callbackInfo = (void *)&ci;
	id.progressCallback = &RunCallback;

	if (!OPTCUDA_Initialize(&id))
	{
		char err[1024];
		OPTCUDA_GetLastError(err, 1024);
		errorString = std::string(err);
		return JNI_FALSE;
	}

	initialised = true;
	return JNI_TRUE;
}

JNIEXPORT jboolean JNICALL Java_uk_co_cairn_CuOPT_CuOPT_1JNI_AddImage(JNIEnv *env, jobject thisObj, jfloatArray jArr, jint offset, jint nSlices)
{
	jfloat *cArr = env->GetFloatArrayElements(jArr, nullptr);
	if (cArr == nullptr)
	{
		errorString = "JNI Error: Could not access image array!";
		return JNI_FALSE;
	}

	if (!OPTCUDA_AddImage(cArr, offset, nSlices))
	{
		char err[1024];
		OPTCUDA_GetLastError(err, 1024);
		errorString = std::string(err);

		env->ReleaseFloatArrayElements(jArr, cArr, 0);
		return JNI_FALSE;
	}

	env->ReleaseFloatArrayElements(jArr, cArr, 0);

	return JNI_TRUE;
}

JNIEXPORT jboolean JNICALL Java_uk_co_cairn_CuOPT_CuOPT_1JNI_Reconstruct(JNIEnv *env, jobject thisObj, jint nSlices)
{
	if (!OPTCUDA_Reconstruct(nSlices))
	{
		char errString[1024];
		OPTCUDA_GetLastError(errString, 1024);
		errorString = std::string(errString);
		return JNI_FALSE;
	}

	return JNI_TRUE;
}

JNIEXPORT jboolean JNICALL Java_uk_co_cairn_CuOPT_CuOPT_1JNI_Deinitialize(JNIEnv *, jobject)
{
	if (!initialised)
	{
		errorString = "JNI Error: Not initialised!";
		return JNI_FALSE;
	}

	return OPTCUDA_Deinitialize() ? JNI_TRUE : JNI_FALSE;
}

JNIEXPORT jstring JNICALL Java_uk_co_cairn_CuOPT_CuOPT_1JNI_GetLastError(JNIEnv *env, jobject)
{
	return env->NewStringUTF(errorString.c_str());
}

JNIEXPORT jfloatArray JNICALL Java_uk_co_cairn_CuOPT_CuOPT_1JNI_GetOutputSlice(JNIEnv *, jobject, jint)
{
	return nullptr;
}

JNIEXPORT jint JNICALL Java_uk_co_cairn_CuOPT_CuOPT_1JNI_GetMaxSlices(JNIEnv *env, jobject thisObj, jint width, jint nTheta)
{
	return OPTCUDA_GetMaxSlices(width, nTheta);
}

