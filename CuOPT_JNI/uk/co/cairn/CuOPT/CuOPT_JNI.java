package uk.co.cairn.CuOPT;

import java.nio.ByteBuffer;

public class CuOPT_JNI {
	static {
		System.loadLibrary("CuOPT_JNI");
	}
	
	public native boolean Initialize(InitData id);
	public native boolean AddImage(float[] imageData, int offset, int nSlices);
	public native boolean Reconstruct(int nSlices);
	public native boolean Deinitialize();
	public native String GetLastError();
	public native float[] GetOutputSlice(int n);
	public native int GetMaxSlices(int width, int nTheta);
	
	private OPT_Callback callback = null;
	public void RegisterCallback(OPT_Callback c) {
		callback = c;
	}
	public void RunCallback(int n) {
		if (callback != null)
			callback.run(n);
	}

	public static abstract class OPT_Callback {
		abstract public void callback(int n);
		
		public void run(int n)
		{
			callback(n);
		}
	}
	
	public static class InitData {
		public int width;
		public int height;
		public int nTheta;
		public int outWidth;
		public int outHeight;
		public ByteBuffer outBuffer;
	}
	
	public static void main(String[] args) {
		ByteBuffer buf = ByteBuffer.allocateDirect(1024);
		
		InitData id = new InitData();
		id.width = 100;
		id.height = 100;
		id.nTheta = 100;
		id.outWidth = 100;
		id.outHeight = 100;
		id.outBuffer = buf;
		
		CuOPT_JNI obj = new CuOPT_JNI();
		
		if (!obj.Initialize(id))
		{
			String err = obj.GetLastError();
			System.out.print(err);
			return;
		}
		
		if (!obj.Deinitialize())
		{
			String err = obj.GetLastError();
			System.out.print(err);
			return;
		}
		
		System.out.print("Success");
		return;
	}
}