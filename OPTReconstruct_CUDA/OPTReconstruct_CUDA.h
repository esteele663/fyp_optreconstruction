#ifndef OPT_RECONSTRUCT_CUDA_H
#define OPT_RECONSTRUCT_CUDA_H

struct OPTCUDA_InitData
{
	unsigned int imWidth;
	unsigned int imHeight;
	unsigned int nTheta;
	unsigned int outWidth;
	unsigned int outHeight;
	void(*progressCallback)(void *, int);
	void *callbackInfo;
	float * output_buffer;
};

__declspec(dllexport) bool OPTCUDA_Initialize(const OPTCUDA_InitData* initData);
__declspec(dllexport) bool OPTCUDA_AddImage(const float const* imageData, const int offset, const int nSlices);
__declspec(dllexport) bool OPTCUDA_Reconstruct(int nSlices);
__declspec(dllexport) bool OPTCUDA_Deinitialize();
__declspec(dllexport) void OPTCUDA_GetLastError(char *errorString, size_t length);
__declspec(dllexport) float *OPTCUDA_GetOutputSlice(int n);
__declspec(dllexport) void OPTCUDA_GetOutputMinMax(float *min, float *max);
__declspec(dllexport) int OPTCUDA_GetMaxSlices(int width, int nTheta);

#endif // !OPT_RECONSTRUCT_CUDA_H
