#define _USE_MATH_DEFINES
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cufft.h>

#include "OPTReconstruct_CUDA.h"

#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <Windows.h>


#define TO_RAD(x) ((x) * M_PI / 180)
#define XY(x, y, width) ((y) * width + (x))
#define XYZ(x, y, z, width, height) ((z) * (width) * (height) + (y) * (width) + (x))

bool initialized = false;

char *errorString = NULL;
size_t errorStringLength = 0;

unsigned int numImages = 0;
unsigned int imWidth = 0;
unsigned int imHeight = 0;
unsigned int nTheta = 0;
float *d_originalImages = NULL;
size_t pitch = 0;
cudaPitchedPtr d_originalImagesPitched;
texture<float,cudaTextureType2DLayered> tex_original;

void(*progressCallback)(void *, int) = NULL;
void *callbackInfo = NULL;

float *d_out = NULL;
float *d_other = NULL;
unsigned int outWidth = 0;
unsigned int outHeight = 0;

float *h_buff = NULL;
float *h_out = NULL;

//TODO: What if image too big for GPU memory?
//TODO: Add arbitrary positions

void OPTCUDA_SetErrorMessageText(const char *str)
{
	int strLen = strlen(str) + 1; //include the null character
	if (errorString)
		free(errorString);

	errorString = (char*)malloc(sizeof(char)*strLen);

	if (errorString)
		memcpy_s(errorString, strLen, str, strLen);
	errorStringLength = strLen;
}

bool OPTCUDA_Initialize(const OPTCUDA_InitData* id)
{
	imWidth = id->imWidth;
	imHeight = id->imHeight;
	progressCallback = id->progressCallback;
	callbackInfo = id->callbackInfo;
	outWidth = id->outWidth;
	outHeight = id->outHeight;
	nTheta = id->nTheta;
	h_out = id->output_buffer;

	cudaError err = cudaMalloc3D(&d_originalImagesPitched, { imWidth * sizeof(float), imHeight, nTheta });
	d_originalImages = (float*)d_originalImagesPitched.ptr;
	pitch = d_originalImagesPitched.pitch;
	if (err != cudaError::cudaSuccess)
	{
		OPTCUDA_SetErrorMessageText("Could not allocate GPU memory for original images");
		return false;
	}

	err = cudaMalloc(&d_out, id->outWidth * id->outHeight * sizeof(float));
	if (err != cudaError::cudaSuccess)
	{
		OPTCUDA_SetErrorMessageText("Could not allocate GPU memory for output images (first buffer)");
		return false;
	}

	err = cudaMalloc(&d_other, id->outWidth * id->outHeight * sizeof(float));
	if (err != cudaError::cudaSuccess)
	{
		OPTCUDA_SetErrorMessageText("Could not allocate GPU memory for output images (second buffer)");
		return false;
	}

	err = cudaMallocHost(&h_buff, outWidth*outHeight * sizeof(float));
	if (err != cudaError::cudaSuccess || h_buff == NULL)
	{
		OPTCUDA_SetErrorMessageText("Could not allocate RAM buffer for output images");
		return false;
	}

	return true;
}

int prevOffset = 0;

bool OPTCUDA_AddImage(const float const* imageData, const int offset, const int nSlices)
{
	cudaError err = cudaMemcpy2D(d_originalImages + (numImages * (pitch/sizeof(float)) * imHeight), pitch,
		imageData + imWidth*offset, imWidth*sizeof(float), sizeof(float)*imWidth, nSlices, cudaMemcpyHostToDevice);

	if (err != cudaError::cudaSuccess)
	{
		OPTCUDA_SetErrorMessageText("Failed to copy image to device!");
		return false;
	}

	//if (numImages == 0)
		//h_out += outWidth * outHeight*(offset - prevOffset);

	prevOffset = offset;

	numImages++;
	return true;
}

__global__ void OPTCUDA_ReconstructSliceF32Tex(float *out, unsigned int z,
	unsigned int inWidth, unsigned int inHeight, unsigned int outWidth, unsigned int outHeight, unsigned int nTheta)
{
	int x = blockIdx.x*blockDim.x + threadIdx.x;
	int y = blockIdx.y*blockDim.y + threadIdx.y;
	float dTheta = 360.0f / nTheta;
	float scale = 1.0f/sqrtf(outWidth * outWidth + outHeight * outHeight);

	/*float _x = (x - outWidth / 2.0f);
	float _y = (y - outHeight / 2.0f);*/
	float _x = (x / (float)outWidth) - 0.5f;
	float _y = (y / (float)outWidth) - 0.5f;

	float total = 0.0f;
	if (x < outWidth && y < outHeight)
	{
		for (int t = 0; t < nTheta; t++)
		{
			float sTheta = sinf(TO_RAD(t*dTheta));
			float cTheta = cosf(TO_RAD(t*dTheta));
			float r = (_x * sTheta - _y * cTheta+ 0.5f) * (inWidth - 1) + 0.5f;

			float tmp = tex2DLayered(tex_original, r, z + 0.5f, t);
			total += tmp;
		}

		out[XY(x, y, outWidth)] = total;
	}
}


__global__ void OPTCUDA_ReconstructSliceF32(const float * in, float *out, unsigned int z,
	unsigned int inWidth, unsigned int inHeight, unsigned int outWidth, unsigned int outHeight, unsigned int nTheta)
{
	int x = blockIdx.x*blockDim.x + threadIdx.x;
	int y = blockIdx.y*blockDim.y + threadIdx.y;
	float dTheta = 360.0f / nTheta;
	float scale = 1.0f/sqrtf(outWidth * outWidth + outHeight * outHeight);

	float _x = (x - outWidth / 2.0f);
	float _y = (y - outHeight / 2.0f);

	float total = 0.0f;
	if (x < outWidth && y < outHeight)
	{
		for (int t = 0; t < nTheta; t++)
		{
			float sTheta = sinf(TO_RAD(t*dTheta));
			float cTheta = cosf(TO_RAD(t*dTheta));
			float r = _x * sTheta - _y * cTheta;
			int ri = (int)roundf((r * scale + 0.5f) * (inWidth - 1)); //Nearest neighbor, try lerp with texture?

			total += in[XYZ(ri, z, t, inWidth, inHeight)];
		}

		out[XY(x, y, outWidth)] = total;
	}
}

__global__ void OPTCUDA_ReconstructSliceU8(const unsigned char* in, float *out, unsigned int z,
	unsigned int inWidth, unsigned int inHeight, unsigned int outWidth, unsigned int outHeight, unsigned int nTheta)
{
	int x = blockIdx.x*blockDim.x + threadIdx.x;
	int y = blockIdx.y*blockDim.y + threadIdx.y;
	float dTheta = 360.0f / nTheta;
	float scale = 1.0f/sqrtf(outWidth * outWidth + outHeight * outHeight);

	float _x = (x - outWidth / 2.0f);
	float _y = (y - outHeight / 2.0f);

	float total = 0.0f;
	if (x < outWidth && y < outHeight)
	{
		for (int t = 0; t < nTheta; t++)
		{
			float sTheta = sinf(TO_RAD(t*dTheta));
			float cTheta = cosf(TO_RAD(t*dTheta));
			float r = _x * sTheta - _y * cTheta;
			int ri = (int)roundf((r * scale + 0.5f) * (inWidth - 1)); //Nearest neighbor, try lerp with texture?

			total += in[XYZ(ri, z, t, inWidth, inHeight)];
		}

		out[XY(x, y, outWidth)] = total;
	}
}

__global__ void OPTCUDA_RamLak(cufftComplex * in, unsigned int width, unsigned int height, unsigned int nTheta)
{
	int idx = blockIdx.x * blockDim.x + threadIdx.x;
	float f = idx % width;
	f /= (width * width);

	if (idx < width * height * nTheta)
	{
		cufftComplex val = in[idx];
		val.x *= f;
		val.y *= f;
		in[idx] = val;
	}
}

bool OPTCUDA_filter()
{
	cufftComplex *d_workSpace;
	int ifftSize = imWidth / 2 + 1;

	cudaError_t err = cudaMalloc(&d_workSpace, ifftSize * imHeight * nTheta * sizeof(cufftComplex));
	if (err != cudaError::cudaSuccess)
	{
		OPTCUDA_SetErrorMessageText("Could not allocate GPU memory for FFT");
		return false;
	}

	int rank = 1;
	int n = imWidth;
	int inembed = (pitch/sizeof(float)) * imHeight*nTheta;
	int istride = 1;
	int idist = (pitch/sizeof(float));
	int batch = imHeight * nTheta;
	int onembed = ((imWidth / 2) + 1) * imHeight *nTheta;
	int ostride = 1;
	int odist = (imWidth / 2) + 1;

	cufftHandle fftPlan, ifftPlan;
	cufftResult res = cufftPlanMany(&fftPlan, rank, &n, &inembed, istride, idist, &onembed, ostride, odist, CUFFT_R2C, batch);
	if (res != CUFFT_SUCCESS)
	{
		OPTCUDA_SetErrorMessageText("Failed to create FFT Plan!");
		cudaFree(d_workSpace);
		return false;
	}
	res = cufftExecR2C(fftPlan, d_originalImages, d_workSpace);
	if (res != CUFFT_SUCCESS)
	{
		OPTCUDA_SetErrorMessageText("Failed to apply FFT!");
		cudaFree(d_workSpace);
		return false;
	}
	cudaDeviceSynchronize();
	res = cufftDestroy(fftPlan);
	if (res != CUFFT_SUCCESS)
	{
		OPTCUDA_SetErrorMessageText("Failed to destroy FFT Plan!");
		cudaFree(d_workSpace);
		return false;
	}
	cudaDeviceSynchronize();

	dim3 gridSize, blockSize;
	blockSize = dim3(1024, 1, 1);
	gridSize = dim3((onembed - 1) / blockSize.x + 1, 1, 1);
	OPTCUDA_RamLak<<<gridSize, blockSize>>>(d_workSpace, odist, imHeight, nTheta);
	
	cudaDeviceSynchronize();
	if ((err = cudaGetLastError()) != cudaSuccess)
	{
		OPTCUDA_SetErrorMessageText("Failed to apply Ram Lak filter!");
		return false;
	}

	res = cufftPlanMany(&ifftPlan, rank, &n, &onembed, ostride, odist, &inembed, istride, idist, CUFFT_C2R, batch);
	if (res != CUFFT_SUCCESS)
	{
		OPTCUDA_SetErrorMessageText("Failed to create IFFT Plan!");
		cudaFree(d_workSpace);
		return false;
	}
	res = cufftExecC2R(ifftPlan, d_workSpace, d_originalImages);
	if (res != CUFFT_SUCCESS)
	{
		OPTCUDA_SetErrorMessageText("Failed to apply IFFT!");
		cudaFree(d_workSpace);
		return false;
	}
	cudaDeviceSynchronize();
	res = cufftDestroy(ifftPlan);
	if (res != CUFFT_SUCCESS)
	{
		OPTCUDA_SetErrorMessageText("Failed to destroy IFFT Plan!");
		cudaFree(d_workSpace);
		return false;
	}
	
	cudaFree(d_workSpace);
	numImages = 0;
	return true;
}

bool OPTCUDA_Reconstruct(int nSlices)
{
	if (!OPTCUDA_filter())
		return false;

	dim3 blockSize, gridSize;
	blockSize.x = 32; blockSize.y = 32; blockSize.z = 1;
	gridSize.x = (outWidth - 1) / blockSize.x + 1;
	gridSize.y = (outHeight - 1) / blockSize.y + 1;

	cudaError_t err;
	cudaStream_t reconStream, copyStream;

	if ((err = cudaStreamCreate(&reconStream)) != cudaError::cudaSuccess)
	{
		OPTCUDA_SetErrorMessageText("Could not create reconstruction CUDA stream!");
		return false;
	}

	if ((err = cudaStreamCreate(&copyStream)) != cudaError::cudaSuccess)
	{
		OPTCUDA_SetErrorMessageText("Could not create copy CUDA stream!");
		return false;
	}

	//cudaExtent extents = make_cudaExtent(imWidth, imHeight, nTheta);
	cudaExtent extents = make_cudaExtent(imWidth, nSlices, nTheta);
	cudaChannelFormatDesc chanFormat = cudaCreateChannelDesc<float>();
	cudaArray *cuArr = NULL;
	err = cudaMalloc3DArray(&cuArr, &chanFormat, extents, cudaArrayLayered);
	if (err != cudaError::cudaSuccess)
	{
		OPTCUDA_SetErrorMessageText("Could not allocate memory for CUDA Texture!");
		return false;
	}

	cudaMemcpy3DParms memcpyParams = { 0 };
	memcpyParams.srcPtr = d_originalImagesPitched;
	memcpyParams.dstArray = cuArr;
	memcpyParams.extent = extents;
	memcpyParams.kind = cudaMemcpyDeviceToDevice;
	if ((err = cudaMemcpy3D(&memcpyParams)) != cudaError::cudaSuccess)
	{
		OPTCUDA_SetErrorMessageText("Could not copy to texture!");
		return false;
	}


	if ((err = cudaBindTextureToArray(&tex_original, cuArr, &chanFormat)) != cudaError::cudaSuccess)
	{
		OPTCUDA_SetErrorMessageText("Could not bind texture!");
		return false;
	}
	tex_original.normalized = false;
	tex_original.filterMode = cudaFilterModeLinear;
	//tex_original.filterMode = cudaFilterModePoint;

	//OPTCUDA_ReconstructSliceF32Tex <<<gridSize, blockSize, 0, reconStream>>>(d_out, 0, imWidth, imHeight, outWidth, outHeight, nTheta);
	OPTCUDA_ReconstructSliceF32Tex <<<gridSize, blockSize, 0, reconStream>>>(d_out, 0, imWidth, nSlices, outWidth, outHeight, nTheta);
	cudaStreamSynchronize(reconStream);
	if ((err = cudaGetLastError()) != cudaError::cudaSuccess)
	{
		OPTCUDA_SetErrorMessageText("Reconstruction Failed!");
		return false;
	}
	for (int i = 1; i < nSlices; i++)
	{
		cudaMemcpyAsync(h_buff, d_out, outWidth*outHeight * sizeof(float), cudaMemcpyDeviceToHost, copyStream);

		float *d_tmp = d_out;
		d_out = d_other;
		d_other = d_tmp;

		//OPTCUDA_ReconstructSliceF32Tex<<<gridSize, blockSize, 0, reconStream>>>(d_out, i, imWidth, imHeight, outWidth, outHeight, nTheta);
		OPTCUDA_ReconstructSliceF32Tex<<<gridSize, blockSize, 0, reconStream>>>(d_out, i, imWidth, nSlices, outWidth, outHeight, nTheta);

		cudaStreamSynchronize(copyStream);
		memcpy_s(h_out + outWidth * outHeight*(i + prevOffset - 1), outHeight*outWidth * sizeof(float), h_buff, outWidth*outHeight * sizeof(float));
		cudaStreamSynchronize(reconStream);

		if (progressCallback != nullptr)
			progressCallback(callbackInfo, i + prevOffset);
		if ((err = cudaGetLastError()) != cudaError::cudaSuccess)
		{
			OPTCUDA_SetErrorMessageText("Reconstruction Failed!");
			return false;
		}
	}

	cudaMemcpyAsync(h_buff, d_out, outWidth*outHeight * sizeof(float), cudaMemcpyDeviceToHost, copyStream);
	cudaMemcpyAsync(h_out + outWidth * outHeight * (prevOffset + nSlices-1), h_buff, outWidth*outHeight * sizeof(float), cudaMemcpyHostToHost, copyStream);
	cudaStreamSynchronize(copyStream);

	if ((err = cudaGetLastError()) != cudaError::cudaSuccess)
	{
		OPTCUDA_SetErrorMessageText("Reconstruction Failed!");
		return false;
	}	

	cudaFreeArray(cuArr);

	return true;
}

float * OPTCUDA_GetOutputSlice(int n)
{
	return h_out + outWidth * outHeight*n;
}

void OPTCUDA_GetOutputMinMax(float *min, float *max)
{
	
}

bool OPTCUDA_Deinitialize()
{
	if (errorString)
		free(errorString);

	if (d_originalImages)
	{
		cudaError err = cudaFree(d_originalImages);
		if (err != cudaError::cudaSuccess)
			return false;
	}

	if (d_out)
	{
		cudaError err = cudaFree(d_out);
		if (err != cudaError::cudaSuccess)
			return false;
	}

	if (d_other)
	{
		cudaError err = cudaFree(d_other);
		if (err != cudaError::cudaSuccess)
			return false;
	}

	if (h_buff)
	{
		cudaError err = cudaFreeHost(h_buff);
		if (err != cudaError::cudaSuccess)
			return false;
	}

	return true;
}

void OPTCUDA_GetLastError(char *out, size_t length)
{
	if (errorString)
		memcpy_s(out, length, errorString, errorStringLength);
	else
		memcpy_s(out, length, "No Error", 8);
}

int OPTCUDA_GetMaxSlices(int width, int nTheta)
{
	size_t max, free;
	cudaMemGetInfo(&free, &max);

	if (width && !(width & (width - 1)))
		return (max / (4 * sizeof(float))) / (width * nTheta);
	else
		return (max / (8 * sizeof(float))) / (width * nTheta);
}